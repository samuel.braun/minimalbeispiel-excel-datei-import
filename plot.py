#!/usr/bin/env python
import pandas as pd
filenames=[]

# Open file in dialog
from tkinter import Tk
from tkinter.filedialog import askopenfilename
Tk().withdraw()
filenames = askopenfilename(title="Choose a file", filetypes=[('xls* files', '*.xls*')],multiple=True)

for file in filenames:
    print(file)
    xl = pd.ExcelFile(file)
    print(xl.sheet_names)  # see all sheet names